# frozen_string_literal: true

module Types
  module ComplianceManagement
    class ComplianceFrameworkFilterInputType < ::Types::ComplianceManagement::ComplianceFrameworkFilterInputBaseType
      graphql_name 'ComplianceFrameworkFilters'
    end
  end
end
